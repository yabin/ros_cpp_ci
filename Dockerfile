FROM ros:kinetic-ros-core

RUN mkdir -p /home/github_rep \
    && cd /home/github_rep \
    && git clone https://github.com/cpplint/cpplint.git \
    && apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y cppcheck